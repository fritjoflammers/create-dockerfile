# This scripts reads the file "annual-temp-anomalies.csv"
# and create a scatter plots for both temperature  sources
# in python

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
data = pd.read_csv("annual-temp-anomalies.csv")


plt.figure(figsize=(8, 6))


sources = data["Source"].unique()  # Get unique sources
for source in sources:
    source_data = data[data["Source"] == source]  # Filter data for each source
    plt.plot(source_data["Year"], source_data["Mean"], label=source)  # Plot line for each source

plt.xlabel("Year")
plt.ylabel("Temperature Anomaly (°C)")
plt.title("Global Temperature Anomalies")
plt.legend()
plt.grid(True)
plt.show()
plt.savefig("temp_plot.png")

